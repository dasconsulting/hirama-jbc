This project contains MATLAB code used in the publication **"Phosphatidylserine dictates the assembly and dynamics of caveolae in the plasma membrane"** by Hirama et al, <a href = "http://www.jbc.org/content/292/34/14292" target = "_blank"> Journal of Biological Chemistry, 292:14292–14307 </a>

![JBC, 292:14292–14307 (2017)](/figures/jbc-2017-title.png)

---

## Overview

In this paper we quantified the spatial distribution of the membrane protein Caveolin. Specifically, we analyzed TIRF images to detect "punctae" - i.e. aggregates of fluorescent Caveolin at the plasma membrane using a spot detector algorithm[^1]. We estimated the total Caveolin content within these punctae, and calculated the relative amount of Caveolin within the punctae versus diffuse at the membrane.

Follow the link to the paper, <a href = "https://www.dasconsulting.io/portfolio/caveolin/" target = "_blank"> or click here for more details </a>

## Contents

This repository contains:

- Sample images - [Data](data/)
- Sample scripts (see below for details)
  - [`single_channel_analysis_script.m`](single_channel_analysis_script.m)
  - [`dual_view_analysis_script.m`](dual_view_analysis_script.m) 
- MATLAB functions used in these scripts - [Matlab](Matlab/)

## Instructions 

### Download

To download the contents of this repository, either use one of the download options at the top of this page, or use the following command at a terminal (assuming you have `git` installed):

``` shell
git clone https://gitlab.com/dasconsulting/hirama-jbc.git
```

### Usage

We provide sample MATLAB scripts that demonstrate the analysis workflow for two different scenarios. The best way to understand the analysis is to run these scripts line-by-line and read the comments to follow along.

#### Workflow 1: Analysis of single-channel TIRF images

The input consists of a TIRF image and a manually generated binary membrane mask as shown below: 

![Inputs for single channel analysis - TIRF image of Cav1-GFP (left) and a binary membrane mask (right)](figures/single-channel-input.png)  
*Inputs for single channel analysis - TIRF image of Cav1-GFP (left) and a binary membrane mask (right)*

The script [`single_channel_analysis_script.m`](single_channel_analysis_script.m) performs the following tasks:

1. Import images 
2. Identify Caveolin aggregates (also referred to as spots or punctae)
3. Compute properties of each identified spot: area, mean pixel intensity and integrated intensity
4. Quantify relative amounts of Cavolin in punctae -vs- diffuse at the membrane 

#### Workflow 2: Analysis of dual view TIRF images

In this case, the input is a dual view stack with the image split into two channels as shown below:

![Input for dual view analysis - membrane label (top) and Cav1-GFP (bottom). The membrane label images are used to generate the membrane mask](figures/dual-view-input.png)  
*Input for dual view analysis - membrane label (top) and Cav1-GFP (bottom). The membrane label images are used to generate the membrane mask*

The top channel (red) is a membrane label, and the bottom channel (green) is Cav1-GFP. The script  [`dual_view_analysis_script.m`](dual_view_analysis_script.m) uses the membrane label to generate a binary membrane mask (see Supplemental Figure 1 in the paper for a details). The analysis then proceeds as in the single channel case. 

#### Saving and displaying results

The output of the analysis can be optionally saved to a csv file.

To display the results, we typically plot the cumulative distribution function (CDF) of integrated spot intensities - the product of spot area and the mean pixel intensity - which is a proxy for the total Caveolin content within an aggregate. The sample scripts show how to estimate the CDF and create such plots.

![Cumulative distribution function (CDF) of integrated spot intensities (solid line) and a 95% confidence band. The solid circle in the middle markes the median value.](figures/single-channel-intensity-cdf.png)  
*Cumulative distribution function (CDF) of integrated spot intensities (solid line) and a 95% confidence band. The solid circle in the middle markes the median value.*

## Questions?

Please file an issue using the link at the top.

[^1]: The spot detector algorithm is from the paper "Extraction of spots in biological images using multiscale products", Jean-Christophe Olivo-Marin, _Pattern Recognition_, 35: 1989-1996 (2002). 

