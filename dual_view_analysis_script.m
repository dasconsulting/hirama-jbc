% Demo script that shows the complete workflow for analyzing a dual view 
% image stack of Caveolin-GFP (green channel)  and a membrane label (red channel). 
% 
% The script performs the following tasks:
% 1. Read a stack of dual view images and split into 2 components channels (membrane label + Caveolin)
% 2. Use membrane label images to construct a membrane mask.
% 3. Use the spot detector algorithm to identify Caveolin aggregates
% 4. Calculate integrated spot intensities.
% 5. Quantify the relative amount of punctate -vs- diffuse Caveolin at the membrane 
%
% Read the comments to follow along
%
% Created by: Raibatak Das
% License: MIT License

% **NOTE**:
% This script assumes that you are currently at the root folder of the repository
% Use pwd to check

addpath('Matlab', 1)
addpath('Matlab/SpotDetector', 1)

%% 1. Import dualview stack and split into component channels

% Import dual view image and split into top and bottom halves
imgFile = 'data/dual-view-stack.tif'
[topStack bottomStack] = import_dualview_stack(imgFile);

% Average membrane channel, extract the first image of the Caveolin channel
membraneImg = uint16(mean(topStack, 3));
cavImg = bottomStack(:, :, 1);

%% 2. Construct binary membrane mask

% Construct a membrane mask from the averaged membrane channel image
membraneMask = create_membrane_mask(membraneImg);

%% 3. Detect spots
[detectionResults, detectionMask] = spotDetector(double(cavImg));

% Refine spots mask
se = strel("disk", 2);
detectionMaskRefined = imopen(detectionMask, se);

% Retain only detected spots contained within membrane mask
spotsMask = logical( immultiply(detectionMaskRefined, membraneMask) );

% Uncomment below to view and save an image of the spots mask
%figure, imshow(spotsMask)
%imwrite(spotsMask, "figures/hela-dv-spots-mask.tif")

%% 4. Compute spot properties (area, mean intensity, integrated intensity)
spotProps = get_spot_props(spotsMask, cavImg);
% spotProps is an n-by-4 array where n = number of detected spots
%  column 1 - Spot id (1, 2,..., n)
%  column 2 - Spot areas in pixels
%  column 3 - Mean spot intensities
%  column 4 - Integrated spot intensities

% Uncomment next line to save results as csv
%csvwrite("results/dual-view-spot-props.csv", spotProps)

% Compute cumulative distribution function (ECDF) of spot intensities
spotIntensities = spotProps(:,4);
[ii f l u] = ecdf95(spotIntensities);

% Plot distribution
figure, stairs(ii, f, 'LineWidth', 2)
set(gca, 'XScale', 'log')
hold on
stairs(ii, l, 'k--')
stairs(ii, u, 'k--')
plot(median(ii), 0.5, 'w.', 'MarkerSize', 48)
plot(median(ii), 0.5, '.', 'MarkerSize', 30)
xlabel('Integrated Intensity')
ylabel('CDF')
grid on
hold off

%% 5. Quantify relative amounts of diffuse versus punctate Caveolin

% How much Caveolin is in punctae?
punctateCavImg = immultiply(spotsMask, cavImg);
punctateCav = sum(double(punctateCavImg(:)))

% How much Caveolin is diffuse
% First construct the inverse of the spots mask
inverseMask = immultiply(imcomplement(spotsMask), membraneMask);
% Apply inverse mask to the Caveolin image 
diffuseCavImg = immultiply(inverseMask, cavImg);
diffuseCav = sum(double(diffuseCavImg(:)))

% What fraction of Caveolin is punctate?
fractionInPunctae = punctateCav/(punctateCav + diffuseCav)

% What is the ratio of diffuse to punctate?
diffuse_ratio = diffuseCav/punctateCav

