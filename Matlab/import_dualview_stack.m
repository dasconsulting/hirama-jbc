function [top bottom] = import_dualview_stack(imgFile)
  % Import a dualview image and split into component channels
  %
  % Created by: Raibatak Das
  % License: MIT License

  imgInfo = imfinfo(imgFile);
  nFrames = length(imgInfo);
  imgWidth = imgInfo(1).Width;
  imgHeight = imgInfo(1).Height;

  % Import images
  fullStack = zeros(imgHeight, imgWidth, nFrames, 'uint16');
  for j = 1:nFrames
    fullStack(:,:,j) = imread(imgFile, 'Index', j);
  end

  % Split into top and bottom halves:
  top = fullStack(1:imgHeight/2, :, :);
  bottom = fullStack(imgHeight/2+1:end, :, :);
end

