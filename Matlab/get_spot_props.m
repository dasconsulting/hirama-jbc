function spotProps = get_spot_props(spotsMask, grayscaleImg)
  % Calculate area, mean intensity and total intensity of each detected spot
  %
  % Inputs
  %  spotsMask: binary mask of detected spots
  %  grayscaleImg: Grayscale caveolin image
  %
  % Output
  %  spotProps: An n-by-4 array where n = number of detected spots
  %    column 1 - Spot id (1, 2,..., n)
  %    column 2 - Spot areas in pixels
  %    column 3 - Mean spot intensities
  %    column 4 - Total spot intensities
  %
  % Created by: Raibatak Das
  % License: MIT License

   cc = bwconncomp(spotsMask);
  features = regionprops(cc, grayscaleImg, 'Area', 'MeanIntensity');
  spotIntensities = [features.Area]' .* [features.MeanIntensity]';
  nSpots = length(features)
  spotProps = [[1:nSpots]' [features.Area]' [features.MeanIntensity]' spotIntensities];
end
