function maskImg = create_membrane_mask(membraneImg)
    % Create a binary mask for the cell body from a grayscale image of a a membrane label
    %
    % Created by: Raibatak Das
    % License: MIT License

    % Segment grayscale image into a binary image
    level = graythresh(membraneImg(:));
    binaryMembraneImg = im2bw(membraneImg, level);

    % Apply morphological closing to smooth out mask
    se = strel('disk', 8);
    prelimMaskImg = imclose(binaryMembraneImg, se);
    
    % Fill holes
    refinedMaskImg = imfill(prelimMaskImg, 'holes');

    % Compute areas of foreground objects. 
    fgObjects = bwconncomp(refinedMaskImg);
    fgAreas = regionprops(fgObjects, 'Area');
    objectAreas = [fgAreas.Area];

    % Retain only the largest connected object as the membrane mask
    [maxArea, index] = max(objectAreas);
    maskImg = false(size(refinedMaskImg));
    maskImg(fgObjects.PixelIdxList{index}) = true;
end
