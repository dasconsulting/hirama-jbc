% Demo script to quantify Caveolin-GFP membrane distribution from a single channel TIRF image.
%
% This script performs the following tasks:
%
% 1. Load a single channel Caveolin (Cav1-GFP) image and a binary membrane mask
% 2. Identify Caveolin aggregates using a spot detector algorithm
% 3. Compute spot sizes and intensities
% 4. Quantify relative amounts of punctate -vs- diffuse Caveolin at the membrane 
%
% Read the comments to follow along
%
% Created by: Raibatak Das
% License: MIT License

% **NOTE 1**
% Lines that save results to a file are commented out. Make sure
% to change the target file names before uncommenting.

% **NOTE 2**:
% This script assumes that you are currently at the root folder of the repository
% Use pwd to check

addpath('Matlab', 1)
addpath('Matlab/SpotDetector', 1)

%% 1. Load images
cavImg = imread("data/single-channel-gfp.tif");
membraneMask = imread("data/single-channel-membrane-mask.tif");

%% 2. Detect spots
[detectionResults, detectionMask] = spotDetector(double(cavImg));

% Refine spots mask
se = strel("disk", 2);
detectionMaskRefined = imopen(detectionMask, se);

% Retain only detected spots contained within membrane mask
spotsMask = logical( immultiply(detectionMaskRefined, membraneMask) );

% Uncomment next block to view and save an image of the spots mask
% figure, imshow(spotsMask)
% imwrite(spotsMask, "figures/cho-spots-mask.tif")

%% 3. Compute spot properties and optionally save results to a csv file
spotProps = get_spot_props(spotsMask, cavImg);
% spotProps is an n-by-4 array where n = number of detected spots
%  column 1 - Spot id (1, 2,..., n)
%  column 2 - Spot areas in pixels
%  column 3 - Mean spot intensities
%  column 4 - Integrated spot intensities

% Uncomment next line to save results as csv
% csvwrite("results/single-channel-spot-props.csv", spotProps)

% Compute cumulative distribution function (ECDF) of spot intensities
spotIntensities = spotProps(:,4);
[ii f l u] = ecdf95(spotIntensities);

% Plot CDF
figure, stairs(ii, f, 'LineWidth', 2)
set(gca, 'XScale', 'log')
hold on
stairs(ii, l, 'k--')
stairs(ii, u, 'k--')
plot(median(ii), 0.5, 'w.', 'MarkerSize', 48)
plot(median(ii), 0.5, '.', 'MarkerSize', 30)
xlabel('Integrated Intensity')
ylabel('CDF')
grid on
hold off

%% 4. Quantify relative amounts of diffuse versus punctate Caveolin

% How much Caveolin is in aggregates?
punctateCavImg = immultiply(spotsMask, cavImg);
punctateCav = sum(double(punctateCavImg(:)))

% How much Caveolin is diffuse?
% First construct the inverse of the spots mask
inverseMask = immultiply(imcomplement(spotsMask), membraneMask);
% Apply inverse mask to the Caveolin image 
diffuseCavImg = immultiply(inverseMask, cavImg);
diffuseCav = sum(double(diffuseCavImg(:)))

% What fraction of Caveolin is punctate?
fractionInPunctae = punctateCav/(punctateCav + diffuseCav)

% What is the ratio of diffuse to punctate?
diffuse_ratio = diffuseCav/punctateCav

% Uncomment these lines to save the punctate and diffuse images
%imwrite(uint16(punctateCavImg), "figures/cho-punctate-cav.tif")
%imwrite(uint16(diffuseCavImg), "figures/cho-diffuse-cav.tif")

